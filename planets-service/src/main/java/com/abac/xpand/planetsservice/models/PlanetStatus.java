package com.abac.xpand.planetsservice.models;

public enum PlanetStatus {
    OK,
    NOK,
    TODO,
    EnRoute;
}
