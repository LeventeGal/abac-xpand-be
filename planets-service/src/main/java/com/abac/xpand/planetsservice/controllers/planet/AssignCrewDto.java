package com.abac.xpand.planetsservice.controllers.planet;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AssignCrewDto {
    private String captain;

    private List<String> robots;
}
