package com.abac.xpand.planetsservice.controllers.planet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusDto {
    private String status;

    private String description;
}
