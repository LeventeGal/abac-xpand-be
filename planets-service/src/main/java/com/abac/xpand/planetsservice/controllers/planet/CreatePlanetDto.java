package com.abac.xpand.planetsservice.controllers.planet;

import com.abac.xpand.planetsservice.models.Planet;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreatePlanetDto {
    private String name;
    private String imageUrl;

    public Planet toPlanet(Planet planet) {
        planet.setName(name);
        planet.setImageUrl(imageUrl);

        return planet;
    }
}
