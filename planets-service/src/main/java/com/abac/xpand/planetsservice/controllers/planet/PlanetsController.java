package com.abac.xpand.planetsservice.controllers.planet;

import com.abac.xpand.planetsservice.models.Planet;
import com.abac.xpand.planetsservice.services.planet.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlanetsController {
    @Autowired
    private PlanetService planetService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Planet>> getAll() {
        final List<Planet> planets = planetService.getAll();

        return ResponseEntity.ok(planets);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody CreatePlanetDto planetVo) {
        planetService.create(planetVo.toPlanet(new Planet()));

        return ResponseEntity.created(null).build();
    }

    @RequestMapping(value = "/{id}/assign-crew", method = RequestMethod.POST)
    public ResponseEntity assignCrew(@PathVariable("id") long planetId, @RequestBody AssignCrewDto crew) {
        final Planet planet = planetService.get(planetId);
        planetService.assignCrew(planet, crew.getCaptain(), crew.getRobots());

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/status", method = RequestMethod.PUT)
    public ResponseEntity updateStatus(@PathVariable("id") long planetId, @RequestBody StatusDto status) {
        final Planet planet = planetService.get(planetId);
        planetService.updateStatus(planet, status.getStatus(), status.getDescription());

        return ResponseEntity.noContent().build();
    }
}
