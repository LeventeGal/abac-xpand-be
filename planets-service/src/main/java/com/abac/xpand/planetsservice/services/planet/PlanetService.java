package com.abac.xpand.planetsservice.services.planet;

import com.abac.xpand.planetsservice.models.Planet;
import com.abac.xpand.planetsservice.models.PlanetStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanetService {
    @Autowired
    private PlanetRepository planetRepository;

    public List<Planet> getAll() {
        return planetRepository.findAll();
    }

    public Planet get(long id) {
        return planetRepository.getOne(id);
    }

    public Planet create(Planet planet) {
        planet.setStatus(PlanetStatus.TODO);

        return planetRepository.saveAndFlush(planet);
    }

    public void assignCrew(Planet planet, String captain, List<String> robots) {
        planet.setCrew(captain, String.join(", ", robots));

        this.planetRepository.saveAndFlush(planet);
    }

    public void updateStatus(Planet planet, String status, String description) {
        planet.setStatus(PlanetStatus.valueOf(status));
        planet.setDescription(description);

        this.planetRepository.saveAndFlush(planet);
    }
}
