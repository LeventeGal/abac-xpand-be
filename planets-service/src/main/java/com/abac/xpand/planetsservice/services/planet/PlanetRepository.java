package com.abac.xpand.planetsservice.services.planet;

import com.abac.xpand.planetsservice.models.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetRepository extends JpaRepository<Planet, Long> {
}
