package com.abac.xpand.planetsservice;

import com.abac.xpand.planetsservice.config.PlanetServiceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan
@EnableJpaRepositories
@Import(PlanetServiceConfig.class)
public class PlanetsService {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "planets-service");

        SpringApplication.run(PlanetsService.class, args);
    }
}


