package com.abac.xpand.planetsservice.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class Crew {
    private String captain;

    private String robots;

    public Crew() {
    }

    Crew(String captain, String robots) {
        this.captain = captain;
        this.robots = robots;
    }
}
