package com.abac.xpand.planetsservice.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
public class Planet {
    @Id
    @GeneratedValue
    private long id;

    @Setter
    private String name;

    @Setter
    private String imageUrl;

    @Setter
    @Enumerated
    private PlanetStatus status;

    @Setter
    private String description;

    @Embedded
    private Crew crew;

    public void setCrew(String captain, String robots) {
        crew = new Crew(captain, robots);
    }
}
