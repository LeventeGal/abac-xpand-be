package com.abac.xpand.webservice.services;

import com.abac.xpand.webservice.models.Crew;
import com.abac.xpand.webservice.models.Planet;
import com.abac.xpand.webservice.models.PlanetStatus;
import com.abac.xpand.webservice.models.SimplePlanet;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WebPlanetsService {
    @Autowired
    @LoadBalanced
    private RestTemplate restTemplate;

    @Value("${services.planetsService}")
    private String serviceUrl;

    public Planet[] getAll() {
        return restTemplate.getForObject(serviceUrl, Planet[].class);
    }

    public void create(SimplePlanet planet) {
        restTemplate.postForEntity(serviceUrl, planet, Void.class);
    }

    public void assignCrew(long planetId, Crew crew) {
        final PlanetServiceCrew planetServiceCrew = new PlanetServiceCrew(crew);

        restTemplate.postForEntity(String.format("%s/%d/assign-crew", serviceUrl, planetId), planetServiceCrew, Void.class);
    }

    public void updateStatus(long planetId, PlanetStatus status) {
        restTemplate.put(String.format("%s/%d/status", serviceUrl, planetId), status, Void.class);
    }

    @Getter
    private static class PlanetServiceCrew {
        private String captain;

        private List<String> robots;

        public PlanetServiceCrew(Crew crew) {
            captain = crew.getCaptain();
            robots = crew.getRobots().stream().map(Crew.Robot::getName).collect(Collectors.toList());
        }
    }
}
