package com.abac.xpand.webservice.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Crew {
    private String captain;

    private List<Robot> robots;

    @Getter
    @Setter
    public static class Robot {
        private String name;
    }
}
