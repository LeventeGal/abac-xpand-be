package com.abac.xpand.webservice.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlanetStatus {
    private String status;

    private String description;
}
