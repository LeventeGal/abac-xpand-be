package com.abac.xpand.webservice.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Planet extends SimplePlanet {

    private String status;

    private String description;

    private Crew crew;

    @Getter
    @Setter
    private static class Crew {
        private String captain;

        private String robots;
    }
}
