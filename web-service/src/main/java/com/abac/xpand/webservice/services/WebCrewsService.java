package com.abac.xpand.webservice.services;

import com.abac.xpand.webservice.models.Crew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class WebCrewsService {
    @Autowired
    @LoadBalanced
    private RestTemplate restTemplate;

    @Value("${services.crewsService}")
    private String serviceUrl;

    public void create(Crew crew) {
        restTemplate.postForEntity(serviceUrl, crew, Void.class);
    }

    public ResponseEntity<Crew[]> getAll() {
        return restTemplate.getForEntity(serviceUrl, Crew[].class);
    }

    public ResponseEntity<Crew[]> search(String searchTerm) {
        final Map<String, String> params = new HashMap<>(1);
        params.put("q", searchTerm);

        return restTemplate.getForEntity(serviceUrl + "?q={q}", Crew[].class, params);
    }
}
