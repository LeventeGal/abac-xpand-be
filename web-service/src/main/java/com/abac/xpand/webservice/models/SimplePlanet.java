package com.abac.xpand.webservice.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimplePlanet {
    private long id;

    private String name;

    private String imageUrl;
}
