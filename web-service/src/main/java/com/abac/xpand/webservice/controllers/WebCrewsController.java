package com.abac.xpand.webservice.controllers;

import com.abac.xpand.webservice.models.Crew;
import com.abac.xpand.webservice.services.WebCrewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/crews")
public class WebCrewsController {
    @Autowired
    private WebCrewsService crewsService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Crew[]> getAll() {

        return ResponseEntity.ok(crewsService.getAll().getBody());
    }

    @RequestMapping(method = RequestMethod.GET, params = {"q"})
    public ResponseEntity<Crew[]> search(@RequestParam("q") String searchTerm) {

        return ResponseEntity.ok(crewsService.search(searchTerm).getBody());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Crew crew) {

        crewsService.create(crew);

        return ResponseEntity.created(null).build();
    }
}
