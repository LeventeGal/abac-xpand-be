package com.abac.xpand.webservice.controllers;

import com.abac.xpand.webservice.models.Crew;
import com.abac.xpand.webservice.models.Planet;
import com.abac.xpand.webservice.models.PlanetStatus;
import com.abac.xpand.webservice.models.SimplePlanet;
import com.abac.xpand.webservice.services.WebPlanetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/planets")
public class WebPlanetsController {

    @Autowired
    private WebPlanetsService planetsService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Planet[]> getAll() {
        final Planet[] planets = planetsService.getAll();

        return ResponseEntity.ok(planets);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Planet[]> create(@RequestBody SimplePlanet planet) {
        planetsService.create(planet);

        return ResponseEntity.created(null).build();
    }

    @RequestMapping(value = "/{id}/assign-crew", method = RequestMethod.POST)
    public ResponseEntity<Planet[]> assignCrew(@PathVariable("id") long planetId, @RequestBody Crew crew) {
        planetsService.assignCrew(planetId, crew);

        return ResponseEntity.created(null).build();
    }

    @RequestMapping(value = "/{id}/status", method = RequestMethod.PUT)
    public ResponseEntity<Planet[]> updateStatus(@PathVariable("id") long planetId, @RequestBody PlanetStatus status) {
        planetsService.updateStatus(planetId, status);

        return ResponseEntity.created(null).build();
    }

}
