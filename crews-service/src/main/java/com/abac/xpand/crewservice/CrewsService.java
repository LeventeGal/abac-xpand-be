package com.abac.xpand.crewservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan
@EnableJpaRepositories
@ComponentScan
public class CrewsService {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "crews-service");

        SpringApplication.run(CrewsService.class, args);
    }
}