package com.abac.xpand.crewservice.controllers.crew;

import com.abac.xpand.crewservice.models.Crew;
import com.abac.xpand.crewservice.models.Robot;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
class CrewVo {
    private String captain;

    private List<RobotVO> robots;

    Crew toCrew(Crew crew) {
        crew.setCaptain(captain);
        crew.setRobots(robots.stream().map(r -> r.toRobot(new Robot())).collect(Collectors.toList()));

        return crew;
    }

    CrewVo fromCrew(Crew crew) {
        this.captain = crew.getCaptain();
        this.robots = crew.getRobots().stream().map(r -> new RobotVO().fromRobot(r)).collect(Collectors.toList());

        return this;
    }

    @Getter
    @Setter
    private static class RobotVO {
        private String name;

        private Robot toRobot(Robot robot) {
            robot.setName(name);

            return robot;
        }

        private RobotVO fromRobot(Robot robot) {
            this.name = robot.getName();

            return this;
        }
    }
}
