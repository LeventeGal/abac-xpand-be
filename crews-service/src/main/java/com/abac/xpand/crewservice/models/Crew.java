package com.abac.xpand.crewservice.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Crew {
    @Id
    @GeneratedValue
    private long id;

    @Getter
    @Setter
    private String captain;

    @Getter
    @Setter
    @OneToMany(targetEntity = Robot.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Robot> robots;
}
