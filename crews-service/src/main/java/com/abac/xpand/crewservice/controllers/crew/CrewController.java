package com.abac.xpand.crewservice.controllers.crew;

import com.abac.xpand.crewservice.models.Crew;
import com.abac.xpand.crewservice.services.crew.CrewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CrewController {
    @Autowired
    private CrewService crewService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CrewVo>> getAll() {
        return ResponseEntity.ok(
                crewService.getAll().stream().map(c -> new CrewVo().fromCrew(c))
                        .collect(Collectors.toList())
        );
    }

    @RequestMapping(method = RequestMethod.GET, params = {"q"})
    public ResponseEntity<List<CrewVo>> search(@RequestParam("q") String searchTerm) {
        return ResponseEntity.ok(
                crewService.search(searchTerm).stream().map(c -> new CrewVo().fromCrew(c))
                        .collect(Collectors.toList())
        );
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody CrewVo data) {
        final Crew crew = data.toCrew(new Crew());

        crewService.create(crew);

        return ResponseEntity.created(null).build();
    }
}
