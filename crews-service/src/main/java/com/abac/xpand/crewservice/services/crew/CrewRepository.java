package com.abac.xpand.crewservice.services.crew;

import com.abac.xpand.crewservice.models.Crew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CrewRepository extends JpaRepository<Crew, Long> {
    @Query("select c from Crew c where c.captain like concat('%', ?1, '%')")
    List<Crew> search(String searchTerm);
}
