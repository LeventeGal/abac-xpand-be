package com.abac.xpand.crewservice.services.crew;

import com.abac.xpand.crewservice.models.Crew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrewService {
    @Autowired
    private CrewRepository crewRepository;

    public Crew create(Crew crew) {
        return crewRepository.saveAndFlush(crew);
    }

    public List<Crew> getAll() {
        return crewRepository.findAll();
    }

    public List<Crew> search(String searchTerm) {
        return crewRepository.search(searchTerm);
    }
}
