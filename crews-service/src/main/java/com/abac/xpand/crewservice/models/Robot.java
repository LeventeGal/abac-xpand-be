package com.abac.xpand.crewservice.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Robot {
    @Id
    @GeneratedValue
    private long id;

    @Getter
    @Setter
    private String name;
}
